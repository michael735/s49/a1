import { useState } from 'react';
import { BrowserRouter,Routes, Route, Navigate } from 'react-router-dom';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFoundPage from './pages/NotFoundPage';
import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';
import {UserProvider} from './UserContext'

function App() {


  const [user, setUser] = useState({
      id: 1,
      isAdmin: true
  })
  return(
    <UserProvider value ={{user, setUser}}>
    <BrowserRouter>
      <AppNavbar/>
      <Routes>
        <Route path="/" element={<Home/>} />
        <Route path="/courses" element={<Courses/>} />
        {
        user.id !== null && user.isAdmin !== null
        ?
        <Route path="/register" element={<Navigate to="/courses" replace />} />
    
        :
        <Route path="/register" element={<Register/>} />
        }
        <Route path="/login" element={<Login/>} />
        <Route path="*" element={<NotFoundPage/>}/>
      </Routes>
      <Footer/>
    </BrowserRouter>
    </UserProvider>
  )
  
}
    

export default App;
