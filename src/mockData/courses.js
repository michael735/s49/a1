let coursesData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt alias ad velit iusto. Repellat quis expedita magni consequatur esse velit eius quibusdam, iste inventore porro. Fugit temporibus beatae nulla atque?",
        price: 25000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python-Djanngo",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt alias ad velit iusto. Repellat quis expedita magni consequatur esse velit eius quibusdam, iste inventore porro. Fugit temporibus beatae nulla atque?",
        price: 35000,
        onOffer: true 
    },
    {
        id: "wdc003",
        name: "Java-Springboot",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt alias ad velit iusto. Repellat quis expedita magni consequatur esse velit eius quibusdam, iste inventore porro. Fugit temporibus beatae nulla atque?",
        price: 45000,
        onOffer: true 
    },
    {
        id: "wdc004",
        name: "NodeJS-ExpressJS",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt alias ad velit iusto. Repellat quis expedita magni consequatur esse velit eius quibusdam, iste inventore porro. Fugit temporibus beatae nulla atque?",
        price: 55000,
        onOffer: false 
    }

]

export default coursesData