import CourseCard from "./../components/CourseCard";
import { Fragment, useContext } from "react";
import coursesData from "./../mockData/courses";
import UserContext from "../UserContext";

export default function Courses(){

    const {user} = useContext(UserContext)

    console.log(user)


    const courses = coursesData.map(course => {
        return <CourseCard key={course.id} courseProp = {course}/> 
    })

    return (<Fragment>{courses}</Fragment>)
}