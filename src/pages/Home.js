import { Fragment } from "react"
import Banner from "./../components/Banner"
import Highlights from "../components/Highlights"



export default function Home(){
    let bannerData = 
    {
        title: "Zuitt Coding Bootcamp",
        text: "Opportunities for everyone,",
        button: "Enroll Now"
        
    }
    return(
        <Fragment>
            <Banner bannerProp={bannerData}/>
            <Highlights/>
        </Fragment>
    )
}